//
//  ScreenEdgeViewController.swift
//  deberGestures
//
//  Created by user1 on 14/7/17.
//  Copyright © 2017 user1. All rights reserved.
//

import UIKit

class ScreenEdgeViewController: UIViewController {

    @IBOutlet weak var edgeTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func acceptButton(_ sender: Any) {
        let edge:String!=String(edgeTextField.text!)
        if edge != nil{
            switch edge{
                case "top","right","left","bottom":
                    performSegue(withIdentifier: "screenEdgeRegionId", sender: self)
                    print("Intervalo correcto")
            default:
                
                print("Debe ingresar uno de las siguientes opciones en minúscula: top, right, bottom or left")
            }
        }else{
            print("Error. Ingrese un valor distinto de nulo")
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination=segue.destination as! ScreenEdgeRegionViewController
        destination.receivedEdge=String(edgeTextField.text!)
    }
}
