//
//  TapViewController.swift
//  deberGestures
//
//  Created by user1 on 5/7/17.
//  Copyright © 2017 user1. All rights reserved.
//

import UIKit

class TapViewController: UIViewController {

    @IBOutlet weak var numberTapsTextField: UITextField!
    
    @IBOutlet weak var numberFingersTextField: UITextField!
    
    @IBOutlet var tapGesture: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func acceptButton(_ sender: Any) {
        let taps:Int! = Int(numberTapsTextField.text!)
        let finger:Int! = Int(numberFingersTextField.text!)
        
        //print(taps)
        //print("finger is \(finger).")
        if taps != nil&&finger != nil{
            switch (taps,finger){
            case (1...100,1...5):
                performSegue(withIdentifier: "tapRegionId", sender: self)
                print("Intervalo correcto")
            default:
                print("Error. Ingrese un valor en el intervalo")
            }
        }else{
            print("Error. Ingrese valores numericos")
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination=segue.destination as! TapRegionViewController
        destination.receivedTap=Int(numberTapsTextField.text!)
        destination.receivedFinger=Int(numberFingersTextField.text!)
        
    }

}
