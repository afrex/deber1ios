//
//  TapRegionViewController.swift
//  deberGestures
//
//  Created by user1 on 5/7/17.
//  Copyright © 2017 user1. All rights reserved.
//

import UIKit

class TapRegionViewController: UIViewController {
    @IBOutlet weak var gestureView: UIView!

    @IBOutlet var tapGesture: UITapGestureRecognizer!
    var receivedTap:Int!
    var receivedFinger:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
tapGesture.numberOfTapsRequired=receivedTap
        tapGesture.numberOfTouchesRequired=receivedFinger
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch=touches.first!
        if touch.tapCount==2{
            
        }
        print("el touch es: \(touches)")
            print("el touch es: \(touch)")
    }*/
    
    
    @IBAction func tapGestureAction(_ sender: Any) {
        if gestureView.backgroundColor==UIColor.black{
            gestureView.backgroundColor=UIColor.green
        }else{
            gestureView.backgroundColor=UIColor.black
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
