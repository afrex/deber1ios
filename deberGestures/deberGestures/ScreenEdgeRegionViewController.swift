//
//  ScreenEdgeRegionViewController.swift
//  deberGestures
//
//  Created by user1 on 14/7/17.
//  Copyright © 2017 user1. All rights reserved.
//

import UIKit

class ScreenEdgeRegionViewController: UIViewController {

    @IBOutlet weak var gestureView: UIView!
    
    
    @IBOutlet var screenEdgeGesture: UIScreenEdgePanGestureRecognizer!
    
    var receivedEdge:String!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenEdgeGesture=UIScreenEdgePanGestureRecognizer(target: self, action: #selector(self.changeColor(_:)))
        switch receivedEdge{
            case "top":
            screenEdgeGesture.edges = .top
            case "right":
            screenEdgeGesture.edges = .right
            case "bottom":
            screenEdgeGesture.edges = .bottom
            case "left":
            screenEdgeGesture.edges = .left
        default:
            screenEdgeGesture.edges = .all
            
        }
        view.addGestureRecognizer(screenEdgeGesture)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func changeColor(_ sender: UIScreenEdgePanGestureRecognizer){
        if sender.state == .ended{
            if gestureView.backgroundColor==UIColor.black{
                gestureView.backgroundColor=UIColor.green
            }else{
                gestureView.backgroundColor=UIColor.black
            }
        }
    }
    @IBAction func screenEdgeAction(_ sender: UIScreenEdgePanGestureRecognizer) {
        /*switch screenEdgeGesture.edges{
        case UIRectEdge.top:
            gestureView.backgroundColor=UIColor.blue
        case UIRectEdge.right:
            gestureView.backgroundColor=UIColor.red
        case UIRectEdge.bottom:
            gestureView.backgroundColor=UIColor.black
        case UIRectEdge.left:
            gestureView.backgroundColor=UIColor.yellow
        default:
            gestureView.backgroundColor=UIColor.black
            
        }*/
        /*if gestureView.backgroundColor==UIColor.black{
            gestureView.backgroundColor=UIColor.green
        }else{
            gestureView.backgroundColor=UIColor.black
        }*/
        
        
    }

}
